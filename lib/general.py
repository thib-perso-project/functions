# -*- coding: utf-8 -*-

#################################################################################
###                                                                           ###
###                                                                           ###
###                                     GENERAL                               ###
###                                                                           ###
###                                                                           ###
#################################################################################


# Making all necessary python imports for library use
import os as __os
import time as __time
import json as __json
import random as __random
import datetime as __datetime
import requests as __requests
import numpy as __np 
import pandas as __pd
import string as __string
import pickle as __pickle

# Making local imports
import time_format as t

# This is to avoid unuseful python warning with pandas
__pd.options.mode.chained_assignment = None


""""
#################################################################################
List of functions : 

 - (i) line
 - (i) parse_json
 - (i) url_to_json
 - (i) write_to_json_file
 - (i) read_file
 - (i) get_time_range_info_from_df
 - (i) save_df_to_excel
 - (i) transfoCol
 - (i) renameDfCol
 - (i) getPctValue
 - (i) star_print
 - (i) get_functions_in_file

Indicators of importance :

(i)    : Important functions
(ii)   : Intermediary used functions
(iii)  : Not really used
(iii) !: To be deleted
 
#################################################################################
"""

def line(display=False):
    """To make easy separations while printing in console"""
    if display == True:
        return print("____________________________________________________________\n")


def parse_json(path):
    """Given a json_file =path loading it and sending a dictionnary back"""
    with open(path) as f:
        data = __json.load(f)
    return data


def url_to_json(url):
    """ Given a web url with a json file, returns a dictionnary""" 
    r = __requests.get(url)
    json_file= r.json()    
    return json_file


def write_to_json_file(path, fileName, data):
    """Saving as a json file in one line"""
    filePathNameWExt = path + fileName + '.json'
    with open(filePathNameWExt, 'w') as fp:
        __json.dump(data, fp)

def read_file(file_name = 'data.txt'):
    """ Example of file name 'data.txt' """
    with open(file_name, "r") as fichier:
        file =  fichier.read()
    return file 

def load_pickle(file_name):
    PICKLE_PATH = parse_json('params.json')['PICKLE_PATH']
    file_path = PICKLE_PATH + file_name
    with open(file_path, 'rb') as pfilef:
        my_pickle = __pickle.load(pfilef)
    return my_pickle

def save_pickle(object_, file_name):
    PICKLE_PATH = parse_json('params.json')['PICKLE_PATH']
    file_path = PICKLE_PATH + file_name
    with open(file_path, 'wb') as pfile:
        __pickle.dump(object_, pfile)

def list_pickle():
    PICKLE_PATH = 'DATA_PATH' + 'pickle/'
    file_list = __os.listdir(PICKLE_PATH)
    pickle_list = [i for i in file_list if '.p' in i]
    print(pickle_list)



def get_time_range_info_from_df(df, display=True, datetime_col=None):
    if not datetime_col:
        start = str(df.index[0])
        end = str(df.index[-1])
    else:
        start = str(df[datetime_col].iloc[0])
        end = str(df[datetime_col].iloc[-1])        
    if display:
        print('Start : ', start, '\t', t.format_to_numeric_timestamp(start))
        print('End   : ', end, '\t', t.format_to_numeric_timestamp(end))
    return start, end



def save_df_to_excel(df, name='my_df_file.xlsx', open=False):
    """This functions takes a dataFrame or list od dataFrames ans save it into an excel with one or multiple sheets.
    Parameters : df (dataframe to be saved), name (the name of the excel file with .xlsx at the end of it)
    """

    # If df is a single dataFrame : saving it into a classic excel file with one sheet
    if type(df) == __pd.core.frame.DataFrame:
        try:
            df.to_excel(name)
            print('File : ' + str(name) + ' has correctly been saved in the current folder')
            if open:
                cmd_ = 'start excel.exe ' + name
                __os.system(cmd_)
        except:
            print('File : ' + str(name) + ' couldn\'t be saved. Might be open. Re-try.')
            # Check errors : could be because xlsx was not added

    # If df is a list of dataFrames : saving each of them in a sheet
    elif type(df) == list:
        list_df = df
        try:
            writer = __pd.ExcelWriter(name)
            compteur = 0
            for i in list_df:
                if len(i) == 1:
                    i[1].to_excel(writer, "Sheet_" + compteur)
                    compteur += 1
                if len(i) == 2:
                    i[1].to_excel(writer, i[0])
            writer.save()
            print('File : ' + str(name) + ' has correctly been saved in the current folder as a list of dataFrames')
        except:
            print('File : ' + str(name) + ' couldn\'t be saved as a list of dataFrames. Might be open. Re-try.')
            # Check errors : could be because xlsx was not added

    # If df is not one of the two defined formats => Error
    else:
        print('File send was not a dataFrame neither')
        # Add an assertion error ?



def transfoCol(ancien, ponctuation=None, accent=None, replacer='_'):
    """Description : simplifie une chaine de caractère en supprimant les majuscules, la ponctuation, les accents et les espaces
    inputs :
        - ancien as string : chaine à modifier
        - ponctuation as list : liste des caractères à retirer
        - accent as dict : dictionnaire des caractères à modifier par un autre
    outputs:
        - string : chaine de caractère modifiée (simplifiée)
    """  
    
    if not ponctuation:
        caracters_to_remove = list(__string.punctuation) + [' ','°']
        ponctuation = {initial:replacer for initial in caracters_to_remove}

    if not accent:
        avec_accent = ['é', 'è', 'ê', 'à', 'ù', 'ç', 'ô', 'î', 'â']
        sans_accent = ['e', 'e', 'e', 'a', 'u', 'c', 'o', 'i', 'a']
        accent = {sans:avec for sans, avec in zip(avec_accent, sans_accent)}
    
    ancien = ancien.lower()
    ancien = ancien.translate(str.maketrans(ponctuation))
    ancien = ancien.translate(str.maketrans(accent))
    double_replacer = replacer + replacer
    while double_replacer in ancien:
        ancien = ancien.replace(double_replacer, replacer)
    
    if ancien[0] ==replacer:
        ancien = ancien[1:]
        
    if ancien[-1] == replacer:
        ancien = ancien[:-1]
    
    return ancien


def renameDfCol(df, replacer='_'):
    """Description : uniformise le nom des colonnes d'un dataframe en retirant les caractères spéciaux/surabondants
    inputs :
        - df as dataFrame : tableau de données dont les colonnes sont à renommer de manière plus simple
    outputs:
        - dataFrame : tableau de données dont les noms de colonnes ont été modifiés
    """  
    rename_dict = {ancien:transfoCol(ancien, replacer=replacer) for ancien in df.columns}
    df_new = df.rename(columns=rename_dict)
    return df_new


def getPctValue(value_, text_=False):
    pct = __np.round(value_ * 100,2)
    if text_:
        return '{} %'.format(pct)
    return pct


def star_print(text, stars=10,length=None, symbol='*'):
    """Affichage d'une ligne avec une valeur au milieu
    Nécessite un texte, variable text as string
    Choix du nombre de caractères, variable length as int
    Choix du nombre d'étoiles avant/après le texte, variable stars as int
    Choix du symbole, variable symbol as string
    """
    if not length:
        return print(symbol*stars, text, symbol*stars)
    
    text_len = len(text)
    if text_len > length:
        return print(symbol*stars, text, symbol*stars)
    
    stars_start = ((length - text_len) / 2) - 1
    if stars_start == int(stars_start):
        return print(symbol*int(stars_start) +  ' ' + text + ' ' + symbol*int(stars_start))
    else:
        stars_start = int(stars_start)
        return print(symbol*stars_start +  ' ' + text + ' ' + symbol*(stars_start+1))


def get_functions_in_file(file_name, print_=False):
    my_file = read_file(file_name=file_name)
    sp = my_file.split('\n')
    def_line = [i[4:].split('(')[0] for i in sp if i[:3]=='def']
    
    if print_:
        print('List of functions')
        for i in def_line:
            print(' - {}'.format(i))
    return def_line



def get_categorical(df, verbose=False):
    cat_data = df.select_dtypes(include='object')
    num_data = df.select_dtypes(exclude='object')

    cat_cols = cat_data.columns.values
    num_cols = num_data.columns.values

    if verbose:
        print('\nCategorical Columns :\n ',cat_cols)
        print('\nNumerical Columns : \n',num_cols)
    return cat_cols, num_cols


def get_pct_empty_df(df):
    return pd.DataFrame({'Emptiness (%)':np.round((df.isnull().sum()/df.shape[0])*100)})



if __name__ == "__main__":
    print("All functions have been loaded.")
    _ = get_functions_in_file('general.py', print_=True)
    

